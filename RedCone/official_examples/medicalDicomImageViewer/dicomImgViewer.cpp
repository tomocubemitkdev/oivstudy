///////////////////////////////////////////////////////////////////////////////
//
// This program is part of the Open Inventor Medical example set.
//
// Open Inventor customers may use this source code to create or enhance
// Open Inventor-based applications.
//
// The medical utility classes are provided as a prebuilt library named
// "fei.inventor.Medical", that can be used directly in an Open Inventor
// application. The classes in the prebuilt library are documented and
// supported by FEI. These classes are also provided as source code.
//
// Please see $OIVHOME/include/Medical/InventorMedical.h for the full text.
//
///////////////////////////////////////////////////////////////////////////////

/*=======================================================================
** DICOM Image Viewer
** Author      : Mike Heck (March 2016)
**=======================================================================*/

/*-----------------------------------------------------------------------
Medical example program.
Purpose : Demonstrate how to render DICOM images with interactive features.
Description : 
This example shows a number of display and interaction techniques combined
to create a (very) basic DICOM image stack viewer.  Features include:

  - DICOM information (patient, study, series, etc) is displayed using
    screen locked annotation text.
  - The image number and number of images are displayed
  - The window level and width values are displayed.
  - Orientation markers are (letters near edge of window) are displayed.
  - Dynamic scale bars are displayed.
  - Mouse buttons
      - In Selection mode (the default):
          - No buttons: Voxel position and value are displayed as mouse moves.
          - Button 1: Image number changes as mouse moves up and down.
          - Button 2: Window level and width change as mouse moves.
          - Wheel   : Changes image number.
      - In Viewing mode (press ESC to toggle):
          - Button 1: Zoom in and out as mouse moves.
          - Button 2: Pan image as mouse moves.
          - Wheel   : Zoom
  - Hot keys:
      - A : Switch to Axial (Transverse) view.
      - C : Switch to Coronal view.
      - S : Switch to Sagittal view.
      - R : Reset window level and width.
      - H : Reset pan and zoom (default view).
-------------------------------------------------------------------------*/

#include <Inventor/Xt/SoXt.h>
#include <Inventor/Xt/viewers/SoXtExaminerViewer.h>
#include <Inventor/Xt/viewers/SoXtPlaneViewer.h>

#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoText3.h>

#include <Inventor/events/SoKeyboardEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <Inventor/SoPickedPoint.h>

#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/details/SoOrthoSliceDetail.h>
#include <VolumeViz/readers/SoVRDicomFileReader.h>

#include <Medical/InventorMedical.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Medical/nodes/DicomInfo.h>
#include <Medical/nodes/Gnomon.h>
#include <Medical/nodes/TextBox.h>
#include <Medical/nodes/SliceOrientationMarkers.h>
#include <Medical/nodes/SliceScaleBar.h>

#include <iostream>

///////////////////////////////////////////////////////////////////////////////

// Data Set
// set your data file path
#define DATA_FILE "../../../../data/Medical/dicomSample/CVH001.dcm"

#define DATA_PATH "../../../../data/Medical/dicomSample"

#define FILE_MASK  "CVH%03d.dcm"
#define FILE_FIRST 1
#define FILE_LAST  557

///////////////////////////////////////////////////////////////////////////////

static void buildAnnotations(SoGroup* root);

// Event handlers
static void onKeyPress(void* data, SoEventCallback* node);
static void onMouseButton(void* data, SoEventCallback* node);
static void onMouseMove(void* data, SoEventCallback* node);
static void onMouseWheel(void* data, SoEventCallback* node);

// Change to a different image
static void goToNewImage(int upOrDown);
// Change to a different axis
static void goToNewAxis(MedicalHelper::Axis axis);

// User interface updaters
static void ui_updateSliceNum(int sliceNum, int numSlices);
static void ui_updateWinCtrWidth(float center, float width);
static void ui_updateVoxelPosVal(const SbVec3i32* ijkPos, float value);

///////////////////////////////////////////////////////////////////////////////
// Application state

// Nodes we might need to query from (or modify) during execution.
static SoXtViewer* m_viewer = nullptr;
static SoVolumeData* m_volData = nullptr;
static SoDataRange* m_dataRange = nullptr;
static SoOrthoSlice* m_sliceNode = nullptr;

static SbVec2f m_resetWinCtrWidth(0, 0); // Init this when data set is loaded.

// TextBox to display dynamic info and corresponding line numbers.
static TextBox* m_infoDisplay = nullptr;
static int m_line_sliceNum = 0;
static int m_line_ctrWidth = 1;
static int m_line_voxValue = 2;

// Slice properties needed for the user interface
static int m_sliceAxis = MedicalHelper::AXIAL;
static int m_sliceNum = 0;
static int m_numSlices = 0;

// Mouse button state and enable display of voxel values
enum MouseMode {
    MOUSE_NOOP,
    // Moving the mouse does nothing
    MOUSE_SHOW_VALUE,
    // Show voxel pos/value as mouse moves (default when no button)
    MOUSE_SCROLL_IMAGE,
    // Change image number as mouse moves up/down (bound to mouse 1)
    MOUSE_CHANGE_WINCW // Change window center/level as mouse moves  (bound to mouse 2)
};

static bool m_mouse1Down = false;
static bool m_mouse2Down = false;
static SbVec2s m_mousePosition(0, 0);
static MouseMode m_mouseMode = MOUSE_SHOW_VALUE;

///////////////////////////////////////////////////////////////////////////////
int main(int, char** argv) {
    // Create the window, initialize OpenIventor and VolumeViz.
    Widget myWindow = SoXt::init(argv[0]);
    SoVolumeRendering::init();
    InventorMedical::init();

    std::cout << "Mouse buttons\n";
    std::cout << "  - In Selection mode (the default):\n";
    std::cout << "      - No buttons: Voxel position and value are displayed as mouse moves.\n";
    std::cout << "      - Button 1: Image number changes as mouse moves up and down.\n";
    std::cout << "      - Button 2: Window level and width change as mouse moves.\n";
    std::cout << "      - Wheel   : Image number.\n";
    std::cout << "  - In Viewing mode (press ESC to toggle):\n";
    std::cout << "      - Button 1: Zoom in and out as mouse moves.\n";
    std::cout << "      - Button 2: Pan image as mouse moves.\n";
    std::cout << "      - Wheel   : Zoom.\n";
    std::cout << "Hot keys:\n";
    std::cout << "  - A : Switch to Axial (Transverse) view.\n";
    std::cout << "  - C : Switch to Coronal view.\n";
    std::cout << "  - S : Switch to Sagittal view.\n";
    std::cout << "  - R : Reset window level and width.\n";
    std::cout << "  - H : Reset pan and zoom (default view).\n";

    // Create the scene graph.
    SoRef<SoSeparator> root = new SoSeparator();

    auto camera = new SoOrthographicCamera();
    root->addChild(camera);

    // Event handling
    auto eventNode = new SoEventCallback();
    eventNode->addEventCallback(SoKeyboardEvent::getClassTypeId(), onKeyPress);
    eventNode->addEventCallback(SoLocation2Event::getClassTypeId(), onMouseMove);
    eventNode->addEventCallback(SoMouseWheelEvent::getClassTypeId(), onMouseWheel);
    eventNode->addEventCallback(SoMouseButtonEvent::getClassTypeId(), onMouseButton);
    root->addChild(eventNode);

    // Volume visualization stuff
    auto volSep = new SoSeparator();
    root->addChild(volSep);

    // Data reader.
    // Generate a list of files from a filename mask and integer sequence.
    // See also: MedicalHelper::dicomFindFilesbySeries()
    auto volReader = new SoVRDicomFileReader();
    SbString filemask = SbString(DATA_PATH) + SbString("/") + SbString(FILE_MASK);
    volReader->setFilenameList(filemask, FILE_FIRST, FILE_LAST);

    // Data node
    auto volData = new SoVolumeData();
    volData->setReader(*volReader);
    MedicalHelper::dicomAdjustVolume(volData);
    root->addChild(volData);

    // Data range (from data file by default)
    auto dataRange = new SoDataRange();
    MedicalHelper::dicomAdjustDataRange(dataRange, volData);
    root->addChild(dataRange);

    // Base material.
    // By default Open Inventor uses gray 0.8 to leave room for lighting to
    // brighten the image. For slice viewing it's better to use full intensity.
    auto volMatl = new SoMaterial();
    volMatl->diffuseColor.setValue(1, 1, 1);
    root->addChild(volMatl);

    // Transfer function (gray scale)
    auto transFunc = new SoTransferFunction();
    transFunc->predefColorMap = SoTransferFunction::INTENSITY;
    MedicalHelper::dicomCheckMonochrome1(transFunc, volData);
    root->addChild(transFunc);

    // Otho slice rendering
    // If we loaded a volume (more than 1 slice) then pick an "interesting"
    // slice in the middle for demoing.  Else we may have loaded a single image.
    int numSlices = volData->data.getSize()[MedicalHelper::AXIAL];
    auto orthoSlice = new SoOrthoSlice();
    orthoSlice->axis = MedicalHelper::AXIAL;
    orthoSlice->sliceNumber = (numSlices > 1) ? (numSlices / 2) : 0;
    orthoSlice->interpolation = SoSlice::MULTISAMPLE_12;
    root->addChild(orthoSlice);

    // OIV Logo
    root->addChild(MedicalHelper::exampleLogoNode());

    // Slice orientation markers.
    // Connect the axis field to the slice so the node will update automatically.
    auto markerSep = new SoSeparator();
    root->addChild(markerSep);
    auto sorientMat = new SoMaterial(); // Color for slice orientation markers
    sorientMat->diffuseColor.setValue(1, 1, 0.25f);
    markerSep->addChild(sorientMat);
    auto sliceOrient = new SliceOrientationMarkers();
    sliceOrient->axis.connectFrom(&orthoSlice->axis);
    markerSep->addChild(sliceOrient);

    // DICOM annotation (including our image number display and so on).
    auto dicomAnnoSwitch = new SoSwitch(); // Allows to toggle annotation visibility
    dicomAnnoSwitch->whichChild = SO_SWITCH_ALL;
    root->addChild(dicomAnnoSwitch);
    buildAnnotations(dicomAnnoSwitch);

    // Dynamic scale bars
    auto scaleSep = new SoSeparator();
    root->addChild(scaleSep);
    auto scaleColor = new SoMaterial();
    scaleColor->diffuseColor.setValue(1, 0.25f, 0.25f);
    scaleSep->addChild(scaleColor);
    auto scaleStyle = new SoDrawStyle();
    scaleStyle->lineWidth = 2;
    scaleSep->addChild(scaleStyle);
    auto scale1 = new SliceScaleBar(); // Horizontal
    scale1->position.setValue(0, -0.99f);
    scale1->length = 100; // 10 cm
    scale1->trackedCamera = camera;
    scale1->label = "10cm";
    scaleSep->addChild(scale1);
    auto scale2 = new SliceScaleBar(); // Vertical
    scale2->position.setValue(-0.99f, 0);
    scale2->length = 100; // 10 cm
    scale2->orientation = SliceScaleBar::VERTICAL;
    scale2->trackedCamera = camera;
    scale2->label = "10cm";
    scaleSep->addChild(scale2);

    // Set up viewer.
    SoXtPlaneViewer* viewer = new SoXtPlaneViewer(myWindow);
    viewer->setDecoration(FALSE);
    viewer->setViewing(FALSE);
    viewer->setTitle("DICOM Image Viewer");
    viewer->setSize(MedicalHelper::exampleWindowSize());
    viewer->setBackgroundColor(SbColor(0, 0.1f, 0.1f));
    viewer->setSceneGraph(root.ptr());

    // Initialize the camera view
    MedicalHelper::orientView(MedicalHelper::AXIAL, viewer->getCamera(), volData);
    viewer->saveHomePosition();
    viewer->show();

    // Set up variables for UI
    m_viewer = viewer;
    m_volData = volData;
    m_dataRange = dataRange;
    m_sliceNode = orthoSlice;
    m_sliceAxis = MedicalHelper::AXIAL;
    m_sliceNum = m_sliceNode->sliceNumber.getValue();
    m_numSlices = volData->data.getSize()[m_sliceAxis];
    m_resetWinCtrWidth = MedicalHelper::dicomGetWindowCenterWidth(m_dataRange);
    ui_updateSliceNum(m_sliceNum, m_numSlices);
    ui_updateWinCtrWidth(m_resetWinCtrWidth[0], m_resetWinCtrWidth[1]);

    SoXt::show(myWindow);
    SoXt::mainLoop();

    // Clean up.
    delete viewer;
    root = nullptr;
    InventorMedical::finish();
    SoVolumeRendering::finish();
    SoXt::finish();
    return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Handle mouse button events --> Affects result of moving the mouse cursor

void onMouseButton(void* /*data*/, SoEventCallback* node) {
    auto evt = static_cast<const SoMouseButtonEvent*>(node->getEvent());

    if (SoMouseButtonEvent::isButtonPressEvent(evt, SoMouseButtonEvent::BUTTON1)) {
        m_mouse1Down = true;
        if (! m_mouse2Down) // Don't change if we're already in a non-default mode.
            m_mouseMode = MOUSE_SCROLL_IMAGE;
    }
    else if (SoMouseButtonEvent::isButtonReleaseEvent(evt, SoMouseButtonEvent::BUTTON1)) {
        m_mouse1Down = false;
        if (! m_mouse2Down) m_mouseMode = MOUSE_SHOW_VALUE; // Default when no button pressed
    }
    else if (SoMouseButtonEvent::isButtonPressEvent(evt, SoMouseButtonEvent::BUTTON2)) {
        m_mouse2Down = true;
        if (! m_mouse1Down) m_mouseMode = MOUSE_CHANGE_WINCW;
    }
    else if (SoMouseButtonEvent::isButtonReleaseEvent(evt, SoMouseButtonEvent::BUTTON2)) {
        m_mouse2Down = false;
        if (! m_mouse1Down) m_mouseMode = MOUSE_SHOW_VALUE; // Default when no button pressed
    }
    else if (SoMouseButtonEvent::isButtonPressEvent(evt, SoMouseButtonEvent::BUTTON3)) {
        node->setHandled();
    }
    else if (SoMouseButtonEvent::isButtonReleaseEvent(evt, SoMouseButtonEvent::BUTTON3)) {
        node->setHandled();
    }
    m_mousePosition = evt->getPosition();
}

///////////////////////////////////////////////////////////////////////////////
// Handle mouse move events --> Display voxel under the cursor.

void onMouseMove(void* /*data*/, SoEventCallback* node) {
    auto evt = static_cast<const SoLocation2Event*>(node->getEvent());

    // Check what mode we are in.
    if (m_mouseMode == MOUSE_SHOW_VALUE) {
        //---------------------------------------------------------------
        // Check if the cursor is over any data objects.
        const SoPickedPoint* pickedPt = node->getPickedPoint();
        if (pickedPt != nullptr) {
            // The cursor is over something interesting (probably the ortho slice
            // because we set all the annotation to be unpickable).
            auto detail = dynamic_cast<const SoOrthoSliceDetail*>(pickedPt->getDetail());
            if (detail != nullptr) {
                // Get picked voxel
                const SbVec3i32& ijkPos = detail->getValueDataPos();
                // Get value of voxel
                const float value = static_cast<float>(detail->getValueD());
                // Update the user interface
                ui_updateVoxelPosVal(&ijkPos, value);
                return;
            }
        }
        ui_updateVoxelPosVal(nullptr, 0);
    }
    else if (m_mouseMode == MOUSE_SCROLL_IMAGE) {
        //---------------------------------------------------------------
        SbVec2s newPos = evt->getPosition();
        int delta = newPos[1] - m_mousePosition[1];
        goToNewImage(delta);
    }
    else if (m_mouseMode == MOUSE_CHANGE_WINCW) {
        //---------------------------------------------------------------
        SbVec2s newPos = evt->getPosition();
        int deltaX = newPos[0] - m_mousePosition[0];
        int deltaY = newPos[1] - m_mousePosition[1];
        SbVec2f winCW = MedicalHelper::dicomGetWindowCenterWidth(m_dataRange);
        winCW[0] += deltaY;
        winCW[1] += deltaX;
        MedicalHelper::dicomSetWindowCenterWidth(m_dataRange, winCW);
        ui_updateWinCtrWidth(winCW[0], winCW[1]);
    }

    // Remember new position
    m_mousePosition = evt->getPosition();
}

///////////////////////////////////////////////////////////////////////////////
// Handle mouse wheel events --> Change the image number.
// For consistency with other applications:
//   - Mouse wheel forward decreases image number.
//   - Mouse wheel backward increases image number.

void onMouseWheel(void* /*data*/, SoEventCallback* node) {
    const SoMouseWheelEvent* evt = (SoMouseWheelEvent*)node->getEvent();
    int delta = evt->getDelta();
    goToNewImage(delta);
}

///////////////////////////////////////////////////////////////////////////////
// Handle key press events.
//
// Note:
//   - When slice axis changes the SliceOrientation object will be updated
//     automatically because its field is connected from the slice's field.
void onKeyPress(void* /*data*/, SoEventCallback* node) {
    const SoKeyboardEvent* evt = (SoKeyboardEvent*)node->getEvent();

    //-----------------------------------------------------------------
    // A : Axial view
    if (SoKeyboardEvent::isKeyPressEvent(evt, SoKeyboardEvent::A)) {
        goToNewAxis(MedicalHelper::AXIAL);
    }
        //-----------------------------------------------------------------
        // C : Coronal view
    else if (SoKeyboardEvent::isKeyPressEvent(evt, SoKeyboardEvent::C)) {
        goToNewAxis(MedicalHelper::CORONAL);
    }
        //-----------------------------------------------------------------
        // H : Home (reset view)
    else if (SoKeyboardEvent::isKeyPressEvent(evt, SoKeyboardEvent::H)) {
        m_viewer->resetToHomePosition();
    }
        //-----------------------------------------------------------------
        // S : Sagittal view
    else if (SoKeyboardEvent::isKeyPressEvent(evt, SoKeyboardEvent::S)) {
        goToNewAxis(MedicalHelper::SAGITTAL);
    }
        //-----------------------------------------------------------------
        // R : Reset window center/width to original values
    else if (SoKeyboardEvent::isKeyPressEvent(evt, SoKeyboardEvent::R)) {
        MedicalHelper::dicomSetWindowCenterWidth(m_dataRange, m_resetWinCtrWidth);
        ui_updateWinCtrWidth(m_resetWinCtrWidth[0], m_resetWinCtrWidth[1]);
    }
}

///////////////////////////////////////////////////////////////////////////////
void buildAnnotations(SoGroup* root) {
    auto dicomMatl = new SoMaterial();
    dicomMatl->diffuseColor.setValue(0.8f, 0.8f, 0.5f);
    root->addChild(dicomMatl);

    auto upperLeft = new DicomInfo();
    upperLeft->fileName = DATA_FILE;
    upperLeft->position.setValue(-0.99f, 0.99f, 0);
    upperLeft->alignmentV = DicomInfo::TOP;
    upperLeft->displayDicomInfo("", 0x0010, 0x0010); // Patient Name
    upperLeft->displayDicomInfo("", 0x0010, 0x0030); // Patient Birth Date
    upperLeft->displayDicomInfo("", 0x0008, 0x1030); // Study Description
    upperLeft->displayDicomInfo("", 0x0008, 0x103E); // Series Description
    root->addChild(upperLeft);

    auto upperRight = new DicomInfo();
    upperRight->fileName = DATA_FILE;
    upperRight->position.setValue(0.99f, 0.99f, 0);
    upperRight->alignmentH = DicomInfo::RIGHT;
    upperRight->alignmentV = DicomInfo::TOP;
    upperRight->textAlignH = DicomInfo::RIGHT;
    upperRight->displayDicomInfo("", 0x0008, 0x0080); // Institution
    upperRight->displayDicomInfo("", 0x0008, 0x0090); // Physician
    upperRight->displayDicomInfo("", 0x0008, 0x1090); // Model name
    root->addChild(upperRight);

    auto lowerRight = new DicomInfo();
    lowerRight->fileName = DATA_FILE;
    lowerRight->position.setValue(0.99f, -0.99f, 0);
    lowerRight->alignmentH = DicomInfo::RIGHT;
    lowerRight->alignmentV = DicomInfo::BOTTOM;
    lowerRight->textAlignH = DicomInfo::RIGHT;
    lowerRight->displayDicomInfo("", 0x0008, 0x0060); // Modality
    lowerRight->displayDicomInfo("mA: ", 0x0018, 0x1151); // X-Ray Tube Current
    lowerRight->displayDicomInfo("kV: ", 0x0018, 0x0060); // KVP (Kilo Voltage Peak)
    lowerRight->displayDicomInfo("", 0x0008, 0x0022); // Acquisition date
    root->addChild(lowerRight);

    // This next TextBox is where we'll display dynamic info like image number,
    // voxel value, etc.  So give it a different color to be more visible.
    auto dynInfoMatl = new SoMaterial();
    dynInfoMatl->diffuseColor.setValue(0.8f, 0.5f, 0.5f);
    root->addChild(dynInfoMatl);

    auto lowerLeft = new TextBox();
    lowerLeft->fontSize = 17; // Slightly larger than default
    lowerLeft->position.setValue(-0.99f, -0.94f, 0); // Leave room for OIV logo
    lowerLeft->alignmentV = TextBox::BOTTOM;
    lowerLeft->addLine(""); // Placeholder for image number
    lowerLeft->addLine(""); // Placeholder for window center/width
    lowerLeft->addLine(""); // Placeholder for voxel pos/value
    root->addChild(lowerLeft);

    // For user interface updates
    m_infoDisplay = lowerLeft;
}

///////////////////////////////////////////////////////////////////////////////
// Change to the next image in stack.
// If delta is negative, increment the image number.
// Else decrement the image number.
void goToNewImage(int delta) {
    if (delta < 0) {
        if (m_sliceNum < (m_numSlices - 1)) {
            m_sliceNum++;
            m_sliceNode->sliceNumber = m_sliceNum;
        }
    }
    else {
        if (m_sliceNum > 0) {
            m_sliceNum--;
            m_sliceNode->sliceNumber = m_sliceNum;
        }
    }
    // Update image number display
    ui_updateSliceNum(m_sliceNum, m_numSlices);
    // This (temporarily) invalidates the voxel value display
    ui_updateVoxelPosVal(nullptr, 0);
}

///////////////////////////////////////////////////////////////////////////////
// Change to a different axis
void goToNewAxis(MedicalHelper::Axis axis) {
    // If we only have 1 slice there is no way to view from other axes. :-)
    if (m_numSlices == 1) {
        std::cerr << "Only 1 slice loaded. Not possible to view other axes.\n";
        return;
    }
    // In each case:
    //   - Remember the new axis.
    //   - Flip the slice to the new axis.
    //   - Orient the camera to the new axis and save new home position.
    //   - Force slice number to be valid for the new axis.
    if (axis == MedicalHelper::AXIAL) {
        m_sliceAxis = MedicalHelper::AXIAL;
        m_sliceNode->axis = m_sliceAxis;
        MedicalHelper::orientView(MedicalHelper::AXIAL, m_viewer->getCamera(), m_volData);
        m_viewer->saveHomePosition();
        m_numSlices = m_volData->data.getSize()[m_sliceAxis];
        if (m_sliceNum > (m_numSlices - 1)) m_sliceNum = m_numSlices - 1;
    }
    else if (axis == MedicalHelper::CORONAL) {
        m_sliceAxis = MedicalHelper::CORONAL;
        m_sliceNode->axis = m_sliceAxis;
        MedicalHelper::orientView(MedicalHelper::CORONAL, m_viewer->getCamera(), m_volData);
        m_viewer->saveHomePosition();
        m_numSlices = m_volData->data.getSize()[m_sliceAxis];
        if (m_sliceNum > (m_numSlices - 1)) m_sliceNum = m_numSlices - 1;
    }
    else if (axis == MedicalHelper::SAGITTAL) {
        m_sliceAxis = MedicalHelper::SAGITTAL;
        m_sliceNode->axis = m_sliceAxis;
        MedicalHelper::orientView(MedicalHelper::SAGITTAL, m_viewer->getCamera(), m_volData);
        m_viewer->saveHomePosition();
        m_numSlices = m_volData->data.getSize()[m_sliceAxis];
        if (m_sliceNum > (m_numSlices - 1)) m_sliceNum = m_numSlices - 1;
    }

    // Update slice number display and invalidate voxel value display.
    ui_updateSliceNum(m_sliceNum, m_numSlices);
    ui_updateVoxelPosVal(nullptr, 0);
}

///////////////////////////////////////////////////////////////////////////////
// Update wherever the UI displays the slice number.
// Remember that OIV numbers slices starting at 0, but medical applications
// typically display the image number starting at 1.
void ui_updateSliceNum(int sliceNum, int numSlices) {
    SbString str;
    str.sprintf("Image  %d  /  %d", sliceNum + 1, numSlices);
    m_infoDisplay->setLine(str, m_line_sliceNum);
}

///////////////////////////////////////////////////////////////////////////////
// Update wherever the UI displays the window center/width.
void ui_updateWinCtrWidth(float center, float width) {
    SbString str;
    str.sprintf("WL: %g  WW: %g", center, width);
    m_infoDisplay->setLine(str, m_line_ctrWidth);
}

///////////////////////////////////////////////////////////////////////////////
// Update wherever the UI displays the voxel position and value.
// If values are not valid, clear the display.
void ui_updateVoxelPosVal(const SbVec3i32* ijkPos, float value) {
    SbString str("");
    if (ijkPos != nullptr) {
        // We have valid values, but which voxel coords are relevant depends on
        // the current slice orientation (Axial, Coronal, Sagittal).
        int i = 0; // Valid for Axial case
        int j = 1;
        if (m_sliceAxis == MedicalHelper::CORONAL) {
            i = 0; // X
            j = 2; // Z
        }
        else if (m_sliceAxis == MedicalHelper::SAGITTAL) {
            i = 1; // Y
            j = 2; // Z
        }
        str.sprintf("Pos: %d , %d  Val: %g", (*ijkPos)[i], (*ijkPos)[j], value);
    }
    m_infoDisplay->setLine(str, m_line_voxValue);
}
