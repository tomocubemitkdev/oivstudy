cmake_minimum_required(VERSION 3.13)

project(ExaminerViewer)

#Header files for external use
set(INTERFACE_HEADERS
)

#Header files for internal use
set(PRIVATE_HEADERS
	
)

#Sources
set(SOURCES	
	exviewer.cpp
)

add_executable(${PROJECT_NAME}
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

target_include_directories(${PROJECT_NAME}
    PUBLIC
		${CURRENT_OIVHOME}/include 	
		${CURRENT_OIVHOME}/src/Inventor/gui/
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_directories(${PROJECT_NAME}
	PRIVATE		
		${CURRENT_OIVHOME}/${OIVARCH_}-$(Configuration)/lib
)

target_link_libraries(${PROJECT_NAME}
	PRIVATE		
		${VIEWER_COMPONENT_LINK_LIBRARY}
		Qt5::Widgets
		Qt5::Gui
		Qt5::Core
)


target_compile_features(${PROJECT_NAME} 
	PUBLIC 
		cxx_std_17
)

target_compile_options(${PROJECT_NAME}
	PUBLIC
		/wd4251
    PRIVATE
        /W4 /WX
)