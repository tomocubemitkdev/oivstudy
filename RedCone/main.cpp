#include <Inventor/Xt/SoXt.h>
#include <Inventor/Xt/SoXtRenderArea.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/nodes/SoSeparator.h>

int main(int /*argc*/, char **argv) {
    Widget myWindow = SoXt::init(argv[0]);
    if(myWindow == nullptr) exit(1);

    SoSeparator *root = new SoSeparator;
    SoPerspectiveCamera *myCam = new SoPerspectiveCamera;
    SoMaterial *myMaterial = new SoMaterial;

    root->ref();
    root->addChild(myCam);
    root->addChild(new SoDirectionalLight);
    myMaterial->diffuseColor.setValue(0.0, 1.0, 0.0);
    root->addChild(myMaterial);
    root->addChild(new SoCone);

    // Create a renderArea in which to see our scene graph
    // The render area will appear within the main window
    SoXtRenderArea *myRenderArea = new SoXtRenderArea(myWindow);

    // Make mycam see everything
    myCam->viewAll(root, myRenderArea->getViewportRegion());

    // put our scene in myrenderarea, change the title
    myRenderArea->setSceneGraph(root);
    myRenderArea->setTitle("Green Cone");
    myRenderArea->show();

    SoXt::show(myWindow);
    SoXt::mainLoop();
}